<?php

namespace Tests\CitePolitique\Sdk\Bundle\DependencyInjection;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Dotenv\Dotenv;
use Tests\CitePolitique\Sdk\Bundle\Kernel\CitePolitiqueAppKernel;
use Twig\Environment;

class HtmlBuilderExtensionTest extends TestCase
{
    public function testServices()
    {
        (new Dotenv())->populate([
            'ENDPOINT_A' => 'http://console-demo.citepolitique.fr',
            'API_TOKEN_A' => 'api_token_a',
        ]);

        $kernel = new CitePolitiqueAppKernel('test', true);
        $kernel->boot();

        // Check rendering
        /** @var Environment $twig */
        $twig = $kernel->getContainer()->get('cite_politique.test.twig');

        $rendered = $twig->render('@templates/index.html.twig', [
            'content' => [
                'time' => 1562269479085,
                'blocks' => [
                    [
                        'type' => 'header',
                        'data' => [
                            'text' => 'Titre',
                            'level' => 3,
                        ],
                    ],
                ],
                'version' => '2.13.0',
            ],
        ]);

        $this->assertSame('<div>
<h3 id="title-1">Titre</h3>


</div>
', $rendered);
    }
}
