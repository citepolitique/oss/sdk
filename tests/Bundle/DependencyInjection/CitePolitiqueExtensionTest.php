<?php

namespace Tests\CitePolitique\Sdk\Bundle\DependencyInjection;

use CitePolitique\Sdk\Client\ClientInterface;
use CitePolitique\Sdk\HtmlBuilder\HtmlBuilderInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Dotenv\Dotenv;
use Tests\CitePolitique\Sdk\Bundle\Kernel\CitePolitiqueAppKernel;

class CitePolitiqueExtensionTest extends TestCase
{
    public function testServices()
    {
        (new Dotenv())->populate([
            'ENDPOINT_A' => 'http://console-demo.citepolitique.fr',
            'API_TOKEN_A' => 'api_token_a',
        ]);

        $kernel = new CitePolitiqueAppKernel('test', true);
        $kernel->boot();

        $container = $kernel->getContainer();

        // Check clients
        foreach ($this->getClients() as $name) {
            $this->assertInstanceOf(
                ClientInterface::class,
                $container->get('cite_politique.test.'.$name),
                'Client "'.$name.'" should be an instance of ClientInterface'
            );
        }

        // Check HTML builder
        $this->assertInstanceOf(HtmlBuilderInterface::class, $container->get('cite_politique.test.html_builder'));
    }

    private function getClients()
    {
        return ['client_a', 'client_b'];
    }
}
