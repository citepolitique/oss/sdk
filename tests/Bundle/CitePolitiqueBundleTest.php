<?php

namespace Tests\CitePolitique\Sdk\Bundle;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpKernel\Kernel;
use Tests\CitePolitique\Sdk\Bundle\Kernel\EmptyAppKernel;
use Tests\CitePolitique\Sdk\Bundle\Kernel\FrameworkAppKernel;

class CitePolitiqueBundleTest extends TestCase
{
    public function provideKernels()
    {
        yield 'empty' => [new EmptyAppKernel('test', true)];
        yield 'framework' => [new FrameworkAppKernel('test', true)];
    }

    /**
     * @dataProvider provideKernels
     */
    public function testBootKernel(Kernel $kernel)
    {
        $kernel->boot();
        $this->assertArrayHasKey('CitePolitiqueBundle', $kernel->getBundles());
    }
}
