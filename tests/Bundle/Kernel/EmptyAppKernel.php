<?php

namespace Tests\CitePolitique\Sdk\Bundle\Kernel;

use CitePolitique\Sdk\Bundle\CitePolitiqueBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class EmptyAppKernel extends Kernel
{
    use AppKernelTrait;

    public function registerBundles()
    {
        return [new CitePolitiqueBundle()];
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
    }
}
