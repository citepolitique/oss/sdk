<?php

namespace Tests\CitePolitique\Sdk\Bundle\Kernel;

use CitePolitique\Sdk\Bundle\CitePolitiqueBundle;
use Symfony\Bundle\FrameworkBundle\FrameworkBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\HttpKernel\Kernel;

class CitePolitiqueAppKernel extends Kernel
{
    use AppKernelTrait;

    public function registerBundles()
    {
        return [new FrameworkBundle(), new TwigBundle(), new CitePolitiqueBundle()];
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(__DIR__.'/config/framework.yaml', 'yaml');
        $loader->load(__DIR__.'/config/twig.yaml', 'yaml');
        $loader->load(__DIR__.'/config/cite_politique.yaml', 'yaml');
        $loader->load(__DIR__.'/config/services.yaml', 'yaml');
    }
}
