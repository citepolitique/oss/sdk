<?php

namespace Tests\CitePolitique\Sdk\Client;

use CitePolitique\Sdk\Client\Client;
use CitePolitique\Sdk\Client\ClientInterface;
use CitePolitique\Sdk\Client\Exception\MaintenanceException;
use CitePolitique\Sdk\Client\Model\Candidate;
use CitePolitique\Sdk\Client\Model\Collection;
use CitePolitique\Sdk\Client\Model\DonationRequest;
use CitePolitique\Sdk\Client\Model\Event;
use CitePolitique\Sdk\Client\Model\EventCategory;
use CitePolitique\Sdk\Client\Model\ExternalEvent;
use CitePolitique\Sdk\Client\Model\Idea;
use CitePolitique\Sdk\Client\Model\IdeaCategory;
use CitePolitique\Sdk\Client\Model\MenuItem;
use CitePolitique\Sdk\Client\Model\NewsletterSubscription;
use CitePolitique\Sdk\Client\Model\Page;
use CitePolitique\Sdk\Client\Model\Pagination;
use CitePolitique\Sdk\Client\Model\Post;
use CitePolitique\Sdk\Client\Model\PostCategory;
use CitePolitique\Sdk\Client\Model\ProgramTopic;
use CitePolitique\Sdk\Client\Model\Project;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class ClientTest extends TestCase
{
    public function testGetProject()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/Project/item.json')),
        ]);

        $project = $client->getProject();

        $this->assertInstanceOf(Project::class, $project);
        $this->assertSame(18, $project->getId());

        $this->assertSame(
            [
                'accounts',
                'settings',
                'pages',
                'posts',
                'newsletter',
                'events',
                'ideas',
                'quorum',
                'forms',
                'integrations',
                'monitoring',
            ],
            $project->getModules()
        );

        $this->assertSame(
            [
                'name' => 'Asnières et vous ?',
                'logo_white' => null,
                'logo_dark' => null,
                'icon' => null,
                'contact_email' => 'titouan.galopin@citepolitique.fr',
                'slogan_main' => 'Asnières et vous ?',
                'slogan_second' => 'Réfléchir et agir pour notre ville, ensemble',
                'intro_title' => 'Un collectif de citoyennes et citoyens engagés',
                'intro_text' => 'Notre objectif : vous offrir la possibilité de vous exprimer librement.',
                'home_image_main' => 'https://citepolitique.fr/res/default/home-main.jpg',
                'home_image_second' => 'https://citepolitique.fr/res/default/home-second.jpg',
                'home_image_third' => 'https://citepolitique.fr/res/default/home-third.jpg',
                'facebook_url' => 'https://www.facebook.com/Asnieresetvous',
                'twitter_url' => 'https://twitter.com/Asnieresetvous',
                'instagram_url' => 'https://www.instagram.com/Asnieresetvous/',
                'youtube_url' => 'https://youtube.com/Asnieresetvous',
                'medium_url' => 'https://medium.com/Asnieresetvous',
                'access_username' => '',
                'access_password' => '',
            ],
            $project->getSettings()
        );

        $this->assertSame(
            [
                'mapbox' => 'pk.eyJ1IjoiY2l0ZXBvbGl0aXF1ZSIsImEiOiJjanl1ZDVpeXcwY3E2M2JuNHFmZ2l0NHdzIn0.nJoUKcYxnPRRRLsv62wVcQ',
                'matomo' => [
                    'piwik_url' => 'https://matomo.com/api/',
                    'site_id' => '1',
                ],
                'typeform' => [
                    'OLJHCR' => [
                        'title' => 'Démarrer avec Cité Politique',
                        'url' => 'https://citepolitique.typeform.com/to/OLJHCR',
                    ],
                ],
            ],
            $project->getIntegrations()
        );

        $pages = $project->getPages();
        $this->assertCount(5, $pages);
        $this->assertArrayHasKey('mentions-legales', $pages);

        $page = $pages['mentions-legales'];
        $this->assertInstanceOf(Page::class, $page);
        $this->assertSame(92, $page->getId());
        $this->assertSame('Mentions légales', $page->getTitle());
        $this->assertSame('mentions-legales', $page->getSlug());
        $this->assertFalse($page->isDeletable());
        $this->assertNull($page->getMetaTitle());
        $this->assertNull($page->getMetaDescription());
        $this->assertSame('2019-08-26 16:57:17', $page->getUpdatedAt()->format('Y-m-d H:i:s'));
        $this->assertSame([
            'time' => 1560094586219,
            'blocks' => [
                [
                    'type' => 'heading',
                    'data' => [
                        'text' => 'Editeur',
                        'level' => 2,
                    ],
                ],
            ],
            'version' => '2.12.3',
        ], $page->getContent());

        $menu = $project->getMenu();
        $this->assertCount(5, $menu);

        $menuItem = $menu[0];
        $this->assertInstanceOf(MenuItem::class, $menuItem);
        $this->assertSame(3, $menuItem->getId());
        $this->assertSame('Events', $menuItem->getLabel());
        $this->assertSame('events', $menuItem->getType());
        $this->assertSame([], $menuItem->getExtra());
        $this->assertFalse($menuItem->getOpenNewTab());

        $menuItem = $menu[1];
        $this->assertInstanceOf(MenuItem::class, $menuItem);
        $this->assertSame(9, $menuItem->getId());
        $this->assertSame('Newsletter', $menuItem->getLabel());
        $this->assertSame('newsletter', $menuItem->getType());
        $this->assertSame([], $menuItem->getExtra());
        $this->assertFalse($menuItem->getOpenNewTab());

        $menuItem = $menu[2];
        $this->assertInstanceOf(MenuItem::class, $menuItem);
        $this->assertSame(12, $menuItem->getId());
        $this->assertSame('Mentions légales', $menuItem->getLabel());
        $this->assertSame('page', $menuItem->getType());
        $this->assertSame(['slug' => 'mentions-legales'], $menuItem->getExtra());
        $this->assertTrue($menuItem->getOpenNewTab());
    }

    public function testGetSitemap()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/Sitemap/sitemap.json')),
        ]);

        $sitemap = $client->getSitemap();

        $this->assertCount(5, $sitemap['pages']);
        $this->assertCount(1, $sitemap['posts']);
        $this->assertCount(4, $sitemap['posts_categories']);
        $this->assertCount(149, $sitemap['events']);
        $this->assertCount(3, $sitemap['events_categories']);

        foreach ($sitemap as $entities) {
            foreach ($entities as $entity) {
                $this->assertArrayHasKey('id', $entity);
                $this->assertArrayHasKey('slug', $entity);
                $this->assertArrayHasKey('updated_at', $entity);
            }
        }
    }

    public function testGetPostsCategories()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/PostCategory/collection.json')),
        ]);

        $categories = $client->getPostsCategories();

        $this->assertInstanceOf(Collection::class, $categories);
        $this->assertCount(4, $categories->getItems());

        /** @var PostCategory $category */
        foreach ($categories->getItems() as $category) {
            $this->assertInstanceOf(PostCategory::class, $category);
            $this->assertIsInt($category->getId());
            $this->assertIsString($category->getName());
            $this->assertIsString($category->getSlug());
            $this->assertIsInt($category->getWeight());
        }

        /** @var PostCategory $category */
        $category = $categories->getItems()[0];
        $this->assertSame(31, $category->getId());
        $this->assertSame('Vos idées', $category->getName());
        $this->assertSame('vos-idees', $category->getSlug());
        $this->assertSame(1, $category->getWeight());
    }

    public function testGetPosts()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/Post/collection-p1.json')),
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/Post/collection-p4.json')),
        ]);

        // Page 1
        $posts = $client->getPosts();

        $this->assertInstanceOf(Collection::class, $posts);
        $this->assertCount(30, $posts->getItems());

        /** @var Post $post */
        foreach ($posts->getItems() as $post) {
            $this->assertInstanceOf(Post::class, $post);
            $this->assertIsInt($post->getId());
            $this->assertIsString($post->getTitle());
            $this->assertIsString($post->getSlug());
            $this->assertNull($post->getContent());
        }

        $post = $posts->getItems()[0];
        $this->assertInstanceOf(Post::class, $post);
        $this->assertSame(171, $post->getId());
        $this->assertSame('Architecto unde non dicta eveniet exercitationem aut porro.', $post->getTitle());
        $this->assertSame('architecto-unde-non-dicta-eveniet-exercitationem-aut-porro', $post->getSlug());
        $this->assertNull($post->getMetaTitle());
        $this->assertNull($post->getMetaDescription());
        $this->assertSame('https://example.com', $post->getImage());
        $this->assertSame('youtube:foobar', $post->getVideo());
        $this->assertSame('2019-07-04 22:18:56', $post->getPublishedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('2019-06-08 18:48:41', $post->getUpdatedAt()->format('Y-m-d H:i:s'));

        $this->assertInstanceOf(Pagination::class, $posts->getPagination());
        $this->assertSame(100, $posts->getPagination()->getTotalItems());
        $this->assertNull($posts->getPagination()->getPrevious());
        $this->assertSame('/api/posts?page=2', $posts->getPagination()->getNext());
        $this->assertSame('/api/posts?page=1', $posts->getPagination()->getFirst());
        $this->assertSame('/api/posts?page=4', $posts->getPagination()->getLast());

        // Page 4
        $posts = $client->getPosts(4);

        $this->assertInstanceOf(Collection::class, $posts);
        $this->assertCount(10, $posts->getItems());

        /** @var Post $post */
        foreach ($posts->getItems() as $post) {
            $this->assertInstanceOf(Post::class, $post);
            $this->assertIsInt($post->getId());
            $this->assertIsString($post->getTitle());
            $this->assertIsString($post->getSlug());
            $this->assertNull($post->getContent());
        }

        $this->assertInstanceOf(Pagination::class, $posts->getPagination());
        $this->assertSame(100, $posts->getPagination()->getTotalItems());
        $this->assertNull($posts->getPagination()->getNext());
        $this->assertSame('/api/posts?page=3', $posts->getPagination()->getPrevious());
        $this->assertSame('/api/posts?page=1', $posts->getPagination()->getFirst());
        $this->assertSame('/api/posts?page=4', $posts->getPagination()->getLast());
    }

    public function testGetPost()
    {
        $client = $this->createClient([
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/Post/item.json')),
        ]);

        $post = $client->getPost(161);

        $this->assertInstanceOf(Post::class, $post);
        $this->assertSame(161, $post->getId());
        $this->assertSame('Atelier #1 : Asnières, la Seine & vous ?', $post->getTitle());
        $this->assertSame('atelier-1-asnieres-la-seine-vous', $post->getSlug());
        $this->assertNull($post->getMetaTitle());
        $this->assertNull($post->getMetaDescription());
        $this->assertNull($post->getImage());
        $this->assertSame('2019-07-04 22:18:56', $post->getPublishedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('2019-07-04 22:41:35', $post->getUpdatedAt()->format('Y-m-d H:i:s'));
    }

    public function testGetEventsCategories()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/EventCategory/collection.json')),
        ]);

        $categories = $client->getEventsCategories();

        $this->assertInstanceOf(Collection::class, $categories);
        $this->assertCount(2, $categories->getItems());

        /** @var EventCategory $category */
        foreach ($categories->getItems() as $category) {
            $this->assertInstanceOf(EventCategory::class, $category);
            $this->assertIsInt($category->getId());
            $this->assertIsString($category->getName());
            $this->assertIsString($category->getSlug());
            $this->assertIsInt($category->getWeight());
        }

        /** @var EventCategory $category */
        $category = $categories->getItems()[0];
        $this->assertSame(15, $category->getId());
        $this->assertSame('Événements officiels', $category->getName());
        $this->assertSame('evenements-officiels', $category->getSlug());
        $this->assertSame(1, $category->getWeight());
    }

    public function testGetExternalEvents()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/ExternalEvent/collection.json')),
        ]);

        $events = $client->getExternalEvents(['75014', '75013']);

        $this->assertInstanceOf(Collection::class, $events);
        $this->assertCount(10, $events->getItems());

        /** @var ExternalEvent $event */
        $event = $events->getItems()[0];
        $this->assertInstanceOf(ExternalEvent::class, $event);
        $this->assertSame(12, $event->getId());
        $this->assertSame('Réunion conviviale de rentrée inter -  comités.', $event->getTitle());
        $this->assertSame('2019-09-17-reunion-conviviale-de-rentree-inter-comites', $event->getSlug());
        $this->assertSame('https://en-marche.fr/evenements/2019-09-17-reunion-conviviale-de-rentree-inter-comites', $event->getUrl());
        $this->assertSame('48.8306350', $event->getLatitude());
        $this->assertSame('2.3567620', $event->getLongitude());
        $this->assertSame('194  avenue de Choisy  -  Café  La  Place', $event->getAddress());
        $this->assertSame('75013', $event->getZipCode());
        $this->assertSame('Paris 13e', $event->getCity());
        $this->assertSame('FR', $event->getCountry());
        $this->assertSame('2019-09-17 18:44:25', $event->getUpdatedAt()->format('Y-m-d H:i:s'));
    }

    public function testGetEvents()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/Event/collection.json')),
        ]);

        $events = $client->getEvents(['category' => 69, 'before' => '2019-08-28 11:00:00']);

        $this->assertInstanceOf(Collection::class, $events);
        $this->assertCount(1, $events->getItems());

        $event = $events->getItems()[0];
        $this->assertInstanceOf(Event::class, $event);
        $this->assertSame(1112, $event->getId());
        $this->assertSame('Meeting de lancement de campagne', $event->getTitle());
        $this->assertSame('meeting-de-lancement-de-campagne', $event->getSlug());
        $this->assertSame('Pourtant, maintenir un tel boulevard urbain n\'est pas une fatalité.', $event->getDescription());
        $this->assertSame('https://example.com', $event->getImage());
        $this->assertNull($event->getUrl());
        $this->assertSame('En savoir plus', $event->getButtonText());
        $this->assertSame('48.9130499', $event->getLatitude());
        $this->assertSame('2.2865548', $event->getLongitude());
        $this->assertSame('1 Place de l\'Hôtel de ville, Asnières-sur-Seine', $event->getAddress());
        $this->assertSame('2019-08-28 15:53:52', $event->getDate()->format('Y-m-d H:i:s'));
        $this->assertSame('2019-08-26 16:57:16', $event->getPublishedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('2019-08-26 16:56:21', $event->getUpdatedAt()->format('Y-m-d H:i:s'));

        $this->assertCount(1, $event->getCategories());
        $this->assertInstanceOf(EventCategory::class, $category = $event->getCategories()[0]);
        $this->assertSame(69, $category->getId());
        $this->assertSame('Événements officiels', $category->getName());
        $this->assertSame('evenements-officiels', $category->getSlug());
        $this->assertSame(1, $category->getWeight());

        $this->assertInstanceOf(Pagination::class, $events->getPagination());
        $this->assertSame(1, $events->getPagination()->getTotalItems());
        $this->assertNull($events->getPagination()->getPrevious());
        $this->assertNull($events->getPagination()->getNext());
        $this->assertNull($events->getPagination()->getFirst());
        $this->assertNull($events->getPagination()->getLast());
    }

    public function testGetEvent()
    {
        $client = $this->createClient([
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/Event/item.json')),
        ]);

        $event = $client->getEvent(16);

        $this->assertInstanceOf(Event::class, $event);
        $this->assertSame(16, $event->getId());
        $this->assertSame('Meeting de lancement de campagne', $event->getTitle());
        $this->assertSame('meeting-de-lancement-de-campagne', $event->getSlug());
        $this->assertSame('Pourtant, maintenir un tel boulevard urbain n\'est pas une fatalité.', $event->getDescription());
        $this->assertSame('https://example.com', $event->getImage());
        $this->assertNull($event->getUrl());
        $this->assertSame('En savoir plus', $event->getButtonText());
        $this->assertSame('48.9130499', $event->getLatitude());
        $this->assertSame('2.2865548', $event->getLongitude());
        $this->assertSame('1 Place de l\'Hôtel de ville, Asnières-sur-Seine', $event->getAddress());
        $this->assertSame('2019-08-06 18:02:58', $event->getDate()->format('Y-m-d H:i:s'));
        $this->assertSame('2019-07-31 00:31:03', $event->getPublishedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('2019-07-31 08:08:35', $event->getUpdatedAt()->format('Y-m-d H:i:s'));

        $this->assertCount(1, $event->getCategories());
        $this->assertInstanceOf(EventCategory::class, $category = $event->getCategories()[0]);
        $this->assertSame(15, $category->getId());
        $this->assertSame('Événements officiels', $category->getName());
        $this->assertSame('evenements-officiels', $category->getSlug());
        $this->assertSame(1, $category->getWeight());
    }

    public function testGetCandidates()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/Candidate/collection.json')),
        ]);

        $candidates = $client->getCandidates();

        $this->assertInstanceOf(Collection::class, $candidates);
        $this->assertCount(17, $candidates->getItems());

        /** @var Candidate $candidate */
        foreach ($candidates->getItems() as $candidate) {
            $this->assertInstanceOf(Candidate::class, $candidate);
            $this->assertIsInt($candidate->getId());
            $this->assertIsString($candidate->getFullName());
            $this->assertIsString($candidate->getSlug());
            $this->assertNull($candidate->getContent());
        }

        $candidate = $candidates->getItems()[0];
        $this->assertInstanceOf(Candidate::class, $candidate);
        $this->assertSame(43, $candidate->getId());
        $this->assertSame('Alexandre Brugères', $candidate->getFullName());
        $this->assertSame('alexandre-brugeres', $candidate->getSlug());
        $this->assertSame('http://localhost/assets/image/c4902940-0cdc-50f9-8607-4e3442d143d5/post-main-image/747b54bb-0d8c-4019-a954-edcdfcb671aa.jpg', $candidate->getImage());
        $this->assertSame('2019-10-13 00:12:37', $candidate->getPublishedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('2019-10-13 18:29:36', $candidate->getUpdatedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('https://www.facebook.com/Asnieresetvous', $candidate->getFacebook());
        $this->assertSame('https://twitter.com/Asnieresetvous', $candidate->getTwitter());
        $this->assertSame('alexandre.brugeres@asnieresetvous.fr', $candidate->getEmail());
    }

    public function testGetCandidate()
    {
        $client = $this->createClient([
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/Candidate/item.json')),
        ]);

        $candidate = $client->getCandidate(43);

        $this->assertInstanceOf(Candidate::class, $candidate);
        $this->assertSame(43, $candidate->getId());
        $this->assertSame('Alexandre Brugères', $candidate->getFullName());
        $this->assertSame('alexandre-brugeres', $candidate->getSlug());
        $this->assertSame('http://localhost/assets/image/c4902940-0cdc-50f9-8607-4e3442d143d5/post-main-image/747b54bb-0d8c-4019-a954-edcdfcb671aa.jpg', $candidate->getImage());
        $this->assertSame('2019-10-13 00:12:37', $candidate->getPublishedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('2019-10-13 18:29:36', $candidate->getUpdatedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('https://www.facebook.com/Asnieresetvous', $candidate->getFacebook());
        $this->assertSame('https://twitter.com/Asnieresetvous', $candidate->getTwitter());
        $this->assertSame('alexandre.brugeres@asnieresetvous.fr', $candidate->getEmail());
        $this->assertIsArray($candidate->getContent());
    }

    public function testGetProgramTopics()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/ProgramTopic/collection.json')),
        ]);

        $programPages = $client->getProgramTopics();

        $this->assertInstanceOf(Collection::class, $programPages);
        $this->assertCount(3, $programPages->getItems());

        /** @var ProgramTopic $programPage */
        foreach ($programPages->getItems() as $programPage) {
            $this->assertInstanceOf(ProgramTopic::class, $programPage);
            $this->assertIsInt($programPage->getId());
            $this->assertIsString($programPage->getTitle());
            $this->assertIsString($programPage->getSlug());
            $this->assertIsString($programPage->getColor());
        }

        $programPage = $programPages->getItems()[0];
        $this->assertInstanceOf(ProgramTopic::class, $programPage);
        $this->assertSame(13, $programPage->getId());
        $this->assertSame('Faire de l\'Europe une puissance verte', $programPage->getTitle());
        $this->assertSame('faire-de-l-europe-une-puissance-verte', $programPage->getSlug());
        $this->assertSame('http://localhost/assets/image/test-project/content-image/571a9a71-36b2-48fe-aaff-ed622c4478bf.jpg', $programPage->getImage());
        $this->assertSame('2020-01-22 15:02:50', $programPage->getPublishedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('2020-01-22 19:01:24', $programPage->getUpdatedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('26b255', $programPage->getColor());
        $this->assertNull($programPage->getProposals());
    }

    public function testGetProgramTopic()
    {
        $client = $this->createClient([
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/ProgramTopic/item.json')),
        ]);

        $programPage = $client->getProgramTopic(20);
        $this->assertInstanceOf(ProgramTopic::class, $programPage);
        $this->assertSame(13, $programPage->getId());
        $this->assertSame('Faire de l\'Europe une puissance verte', $programPage->getTitle());
        $this->assertSame('faire-de-l-europe-une-puissance-verte', $programPage->getSlug());
        $this->assertSame('http://localhost/assets/image/test-project/content-image/571a9a71-36b2-48fe-aaff-ed622c4478bf.jpg', $programPage->getImage());
        $this->assertSame('2020-01-22 15:02:50', $programPage->getPublishedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('2020-01-22 19:01:24', $programPage->getUpdatedAt()->format('Y-m-d H:i:s'));
        $this->assertSame('26b255', $programPage->getColor());
        $this->assertCount(3, $programPage->getProposals());
    }

    public function testCreateNewsletterSubscription()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse('{}', ['http_code' => 202]),
        ]);

        $this->assertTrue($client->createNewsletterSubscription(
            new NewsletterSubscription('Titouan', 'Galopin', 'galopintitouan@gmail.com')
        ));
    }

    public function testGetIdeasCategories()
    {
        $client = $this->createClient([
            new MockResponse(file_get_contents(__DIR__.'/Fixtures/IdeaCategory/collection.json')),
        ]);

        $categories = $client->getIdeasCategories();

        $this->assertInstanceOf(Collection::class, $categories);
        $this->assertCount(5, $categories->getItems());

        /** @var IdeaCategory $category */
        foreach ($categories->getItems() as $category) {
            $this->assertInstanceOf(IdeaCategory::class, $category);
            $this->assertIsInt($category->getId());
            $this->assertIsString($category->getName());
            $this->assertIsString($category->getSlug());
            $this->assertIsInt($category->getWeight());
        }

        $category = $categories->getItems()[0];
        $this->assertInstanceOf(IdeaCategory::class, $category);
        $this->assertSame(44, $category->getId());
        $this->assertSame('Une ville animée', $category->getName());
        $this->assertSame('une-ville-animee', $category->getSlug());
        $this->assertSame(1, $category->getWeight());
        $this->assertSame(
            'https://console.citepolitique.fr/assets/image/test-project/post-main-image/4e248fcb-abb5-4b25-b1c8-1d7c486aa434.jpg',
            $category->getImage()
        );

        $category = $categories->getItems()[1];
        $this->assertInstanceOf(IdeaCategory::class, $category);
        $this->assertSame(45, $category->getId());
        $this->assertSame('Une ville verte', $category->getName());
        $this->assertSame('une-ville-verte', $category->getSlug());
        $this->assertSame(2, $category->getWeight());
        $this->assertNull($category->getImage());
    }

    public function testCreateIdea()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse('{}', ['http_code' => 202]),
        ]);

        $this->assertTrue($client->createIdea(new Idea(
            'Titouan',
            'Galopin',
            'galopintitouan@gmail.com',
            'Titre',
            'Contenu',
            false,
            false,
            ['ecologie']
        )));
    }

    public function testCreateDonation()
    {
        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse('{}', ['http_code' => 202]),
        ]);

        $this->assertTrue($client->createDonation(new DonationRequest(
            20,
            'monsieur',
            'Titouan',
            'Galopin',
            'Française',
            '49',
            'Rue de Ponthieu',
            '75008',
            'Paris',
            'France',
            '+33606060606',
            'galopintitouan@gmail.com',
            true,
            ['honeypot' => 'foobar']
        )));
    }

    public function testRetryFailure()
    {
        $this->expectException(MaintenanceException::class);

        $client = $this->createClient([
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse('{}', ['http_code' => 503]),
            new MockResponse('{}', ['http_code' => 503]),
        ]);

        $client->createNewsletterSubscription(
            new NewsletterSubscription('Titouan', 'Galopin', 'galopintitouan@gmail.com')
        );
    }

    private function createClient(array $mockResponses): ClientInterface
    {
        return new Client(new MockHttpClient($mockResponses), 'https://citepolitique.demo', 'project_id');
    }
}
