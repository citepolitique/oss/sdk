<?php

namespace Tests\CitePolitique\Sdk\HtmlBuilder;

use CitePolitique\Sdk\HtmlBuilder\Handler\DelimiterBlockHandler;
use CitePolitique\Sdk\HtmlBuilder\Handler\FacebookBlockHandler;
use CitePolitique\Sdk\HtmlBuilder\Handler\HeaderBlockHandler;
use CitePolitique\Sdk\HtmlBuilder\Handler\ImageBlockHandler;
use CitePolitique\Sdk\HtmlBuilder\Handler\InstagramBlockHandler;
use CitePolitique\Sdk\HtmlBuilder\Handler\ListBlockHandler;
use CitePolitique\Sdk\HtmlBuilder\Handler\ParagraphBlockHandler;
use CitePolitique\Sdk\HtmlBuilder\Handler\QuoteBlockHandler;
use CitePolitique\Sdk\HtmlBuilder\Handler\RawBlockHandler;
use CitePolitique\Sdk\HtmlBuilder\Handler\TwitterBlockHandler;
use CitePolitique\Sdk\HtmlBuilder\HtmlBuilder;
use PHPUnit\Framework\TestCase;

class HtmlBuilderTest extends TestCase
{
    public function testBuildHtml()
    {
        $builder = new HtmlBuilder([
            new DelimiterBlockHandler(),
            new FacebookBlockHandler(),
            new HeaderBlockHandler(),
            new ImageBlockHandler(),
            new InstagramBlockHandler(),
            new ListBlockHandler(),
            new ParagraphBlockHandler(),
            new QuoteBlockHandler(),
            new RawBlockHandler(),
            new TwitterBlockHandler(),
        ]);

        $html = $builder->buildHtml(json_decode(file_get_contents(__DIR__.'/Fixtures/content.json'), true));

        $this->assertSame(trim(file_get_contents(__DIR__.'/Fixtures/expected.html')), trim($html));
    }
}
