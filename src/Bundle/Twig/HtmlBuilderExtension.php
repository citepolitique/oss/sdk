<?php

namespace CitePolitique\Sdk\Bundle\Twig;

use CitePolitique\Sdk\HtmlBuilder\HtmlBuilderInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class HtmlBuilderExtension extends AbstractExtension
{
    private $builder;

    public function __construct(HtmlBuilderInterface $builder)
    {
        $this->builder = $builder;
    }

    public function getFilters()
    {
        return [
            new TwigFilter('build_html', [$this, 'buildHtml'], ['is_safe' => ['html']]),
        ];
    }

    public function buildHtml(array $content): string
    {
        return $this->builder->buildHtml($content);
    }
}
