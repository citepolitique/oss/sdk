<?php

namespace CitePolitique\Sdk\Bundle\Twig;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class SocialLinksExtension extends AbstractExtension
{
    private $urlGenerator;

    public function __construct(UrlGeneratorInterface $urlGenerator = null)
    {
        $this->urlGenerator = $urlGenerator;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('facebook_url', [$this, 'generateFacebookUrl']),
            new TwigFunction('twitter_url', [$this, 'generateTwitterUrl']),
            new TwigFunction('mail_url', [$this, 'generateMailUrl']),
            new TwigFunction('telegram_url', [$this, 'generateTelegramUrl']),
            new TwigFunction('whatsapp_url', [$this, 'generateWhatsAppUrl']),
        ];
    }

    public function generateFacebookUrl(string $name, array $params = []): string
    {
        return 'https://www.facebook.com/sharer/sharer.php?u='.urlencode($this->generateUrlToShare($name, $params));
    }

    public function generateTwitterUrl(string $name, array $params = []): string
    {
        return 'https://twitter.com/intent/tweet?text='.$this->generateUrlToShare($name, $params);
    }

    public function generateMailUrl(string $name, array $params = []): string
    {
        return 'mailto:?body='.$this->generateUrlToShare($name, $params);
    }

    public function generateTelegramUrl(string $name, array $params = []): string
    {
        return 'https://telegram.me/share/url?url='.$this->generateUrlToShare($name, $params);
    }

    public function generateWhatsAppUrl(string $name, array $params = []): string
    {
        return 'https://wa.me/?text='.$this->generateUrlToShare($name, $params);
    }

    private function generateUrlToShare(string $name, array $params = []): string
    {
        if (!$this->urlGenerator) {
            throw new \RuntimeException('No URL generator was found.');
        }

        return $this->urlGenerator->generate($name, $params, UrlGeneratorInterface::ABSOLUTE_URL);
    }
}
