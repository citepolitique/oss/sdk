<?php

namespace CitePolitique\Sdk\Bundle\DependencyInjection;

use CitePolitique\Sdk\Bundle\Twig\HtmlBuilderExtension;
use CitePolitique\Sdk\Bundle\Twig\SocialLinksExtension;
use CitePolitique\Sdk\Client\Client;
use CitePolitique\Sdk\Client\ClientInterface;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Twig\Environment;

class CitePolitiqueExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        // Register clients
        foreach ($config['clients'] as $clientName => $clientConfig) {
            $container->setDefinition($clientName, $this->createClientDefinition($clientConfig));
            $container->registerAliasForArgument($clientName, ClientInterface::class, $clientName)->setPublic(false);
        }

        // Register Twig extensions only if Twig is installed
        if (class_exists(Environment::class)) {
            // HtmlBuilder
            $extension = new Definition(HtmlBuilderExtension::class, [new Reference('cite_politique.html_builder')]);
            $extension->addTag('twig.extension');

            $container->setDefinition('cite_politique.html_builder.twig_extension', $extension);

            // Social links
            $extension = new Definition(SocialLinksExtension::class, [new Reference('router', ContainerInterface::NULL_ON_INVALID_REFERENCE)]);
            $extension->addTag('twig.extension');

            $container->setDefinition('cite_politique.social_links.twig_extension', $extension);
        }
    }

    private function createClientDefinition(array $config)
    {
        $definition = new Definition(Client::class);
        $definition->setPublic(false);
        $definition->setArgument(0, new Reference('http_client'));
        $definition->setArgument(1, $config['endpoint']);
        $definition->setArgument(2, $config['api_token']);
        $definition->addTag('cite_politique.client');

        return $definition;
    }
}
