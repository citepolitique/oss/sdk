<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\EventCategory;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class EventCategoryDenormalizer implements DenormalizerInterface
{
    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset($data['@type']) && 'EventCategory' === $data['@type'];
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return new EventCategory($data['id'], $data['name'], $data['slug'], $data['weight']);
    }
}
