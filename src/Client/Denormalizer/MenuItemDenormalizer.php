<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\MenuItem;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class MenuItemDenormalizer implements DenormalizerInterface
{
    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset($data['@type']) && 'MenuItem' === $data['@type'];
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return new MenuItem($data['id'], $data['label'], $data['type'], $data['openNewTab'], $data['extra'] ?? []);
    }
}
