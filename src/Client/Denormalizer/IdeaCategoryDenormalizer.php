<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\IdeaCategory;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class IdeaCategoryDenormalizer implements DenormalizerInterface
{
    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset($data['@type']) && 'IdeaCategory' === $data['@type'];
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return new IdeaCategory($data['id'], $data['name'], $data['slug'], $data['weight'], $data['image'] ?? null);
    }
}
