<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\ProgramProposal;
use CitePolitique\Sdk\Client\Model\ProgramTopic;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ProgramTopicDenormalizer implements DenormalizerInterface
{
    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset($data['@type']) && 'ProgramTopic' === $data['@type'];
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return new ProgramTopic(
            $data['id'],
            $data['title'],
            $data['slug'],
            isset($data['proposals']) ? $this->createProposals($data['proposals']) : null,
            $data['position'],
            $data['image'],
            $data['color'],
            new \DateTime($data['updatedAt']),
            $data['publishedAt'] ? new \DateTime($data['publishedAt']) : null
        );
    }

    private function createProposals(array $rows): array
    {
        $proposals = [];
        foreach ($rows as $row) {
            $proposals[] = new ProgramProposal(
                $row['id'],
                $row['title'],
                $row['content'],
                $row['position'],
                new \DateTime($row['updatedAt'])
            );
        }

        return $proposals;
    }
}
