<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\Post;
use CitePolitique\Sdk\Client\Model\PostCategory;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class PostDenormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset($data['@type']) && 'Post' === $data['@type'];
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $categories = [];
        foreach ($data['categories'] as $category) {
            $categories[] = $this->denormalizer->denormalize($category, PostCategory::class);
        }

        return new Post(
            $data['id'],
            $data['title'],
            $data['slug'],
            $data['description'] ?? null,
            $data['metaTitle'],
            $data['metaDescription'],
            $data['content'] ?? null,
            $data['image'],
            $data['video'] ?? null,
            $categories,
            new \DateTime($data['updatedAt']),
            $data['publishedAt'] ? new \DateTime($data['publishedAt']) : null
        );
    }
}
