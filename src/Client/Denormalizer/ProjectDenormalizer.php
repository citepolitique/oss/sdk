<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\Project;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ProjectDenormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return 'Project' === $type;
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $pages = [];
        foreach ($data['pages'] ?? [] as $row) {
            $page = $this->denormalizer->denormalize($row, 'Page');
            $pages[$page->getSlug()] = $page;
        }

        $menuItems = [];
        foreach ($data['menu'] ?? [] as $row) {
            $menuItems[] = $this->denormalizer->denormalize($row, 'MenuItem');
        }

        return new Project($data['id'], $data['modules'], $data['settings'], $data['integrations'], $pages, $menuItems);
    }
}
