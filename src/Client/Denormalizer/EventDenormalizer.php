<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\Event;
use CitePolitique\Sdk\Client\Model\EventCategory;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class EventDenormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset($data['@type']) && 'Event' === $data['@type'];
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $categories = [];
        foreach ($data['categories'] as $category) {
            $categories[] = $this->denormalizer->denormalize($category, EventCategory::class);
        }

        return new Event(
            $data['id'],
            $data['title'],
            $data['slug'],
            $data['description'] ?? null,
            $data['image'] ?? null,
            $data['date'] ? new \DateTime($data['date']) : null,
            $data['url'] ?? null,
            $data['buttonText'] ?? null,
            $data['latitude'] ?? null,
            $data['longitude'] ?? null,
            $data['address'] ?? null,
            $categories,
            new \DateTime($data['updatedAt']),
            $data['publishedAt'] ? new \DateTime($data['publishedAt']) : null
        );
    }
}
