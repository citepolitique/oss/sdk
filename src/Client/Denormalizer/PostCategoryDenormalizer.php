<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\PostCategory;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class PostCategoryDenormalizer implements DenormalizerInterface
{
    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset($data['@type']) && 'PostCategory' === $data['@type'];
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return new PostCategory($data['id'], $data['name'], $data['slug'], $data['weight']);
    }
}
