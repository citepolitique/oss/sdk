<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\ExternalEvent;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ExternalEventDenormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset($data['@type']) && 'ExternalEvent' === $data['@type'];
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return new ExternalEvent(
            $data['id'],
            $data['source'],
            $data['sourceId'],
            $data['title'],
            $data['slug'],
            $data['date'] ? new \DateTime($data['date']) : null,
            $data['url'] ?? null,
            $data['latitude'] ?? null,
            $data['longitude'] ?? null,
            $data['address'] ?? null,
            $data['zipCode'] ?? null,
            $data['city'] ?? null,
            $data['country'] ?? null,
            new \DateTime($data['updatedAt'])
        );
    }
}
