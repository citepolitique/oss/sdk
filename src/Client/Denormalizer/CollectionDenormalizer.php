<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\Collection;
use CitePolitique\Sdk\Client\Model\Pagination;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class CollectionDenormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset($data['@type']) && 'hydra:Collection' === $data['@type'];
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $items = [];
        foreach ($data['hydra:member'] as $member) {
            $items[] = $this->denormalizer->denormalize($member, $member['@type']);
        }

        return new Collection($items, new Pagination(
            $data['hydra:totalItems'] ?? \count($items),
            $data['hydra:view']['hydra:first'] ?? null,
            $data['hydra:view']['hydra:last'] ?? null,
            $data['hydra:view']['hydra:previous'] ?? null,
            $data['hydra:view']['hydra:next'] ?? null
        ));
    }
}
