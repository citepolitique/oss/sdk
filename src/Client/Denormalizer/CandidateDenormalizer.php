<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\Candidate;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class CandidateDenormalizer implements DenormalizerInterface
{
    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset($data['@type']) && 'Candidate' === $data['@type'];
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return new Candidate(
            $data['id'],
            $data['fullName'],
            $data['slug'],
            $data['content'] ?? null,
            $data['description'] ?? null,
            $data['position'],
            $data['image'],
            $data['facebook'],
            $data['twitter'],
            $data['email'],
            new \DateTime($data['updatedAt']),
            $data['publishedAt'] ? new \DateTime($data['publishedAt']) : null
        );
    }
}
