<?php

namespace CitePolitique\Sdk\Client\Denormalizer;

use CitePolitique\Sdk\Client\Model\Page;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class PageDenormalizer implements DenormalizerInterface
{
    public function supportsDenormalization($data, $type, $format = null)
    {
        return isset($data['@type']) && 'Page' === $data['@type'];
    }

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        return new Page(
            $data['id'],
            $data['title'],
            $data['slug'],
            $data['metaTitle'],
            $data['metaDescription'],
            $data['deletable'],
            $data['content'],
            new \DateTime($data['updatedAt'])
        );
    }
}
