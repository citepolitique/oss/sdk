<?php

namespace CitePolitique\Sdk\Client;

use CitePolitique\Sdk\Client\Exception\MaintenanceException;
use Symfony\Component\HttpClient\ScopingHttpClient;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class Client implements ClientInterface
{
    private $httpClient;
    private $serializer;

    public function __construct(HttpClientInterface $httpClient, string $baseUrl, string $projectId, SerializerInterface $serializer = null)
    {
        $this->httpClient = new ScopingHttpClient(
            $httpClient,
            [
                '.+' => [
                    'auth_basic' => ['api', $projectId],
                    'base_uri' => $baseUrl,
                    'http_version' => '1.1',
                    'headers' => [
                        'Accept' => 'application/ld+json',
                    ],
                ],
            ],
            '.+'
        );

        $this->serializer = $serializer ?: new Serializer(
            [
                new Denormalizer\CollectionDenormalizer(),
                new Denormalizer\PageDenormalizer(),
                new Denormalizer\MenuItemDenormalizer(),
                new Denormalizer\PostDenormalizer(),
                new Denormalizer\PostCategoryDenormalizer(),
                new Denormalizer\ExternalEventDenormalizer(),
                new Denormalizer\EventDenormalizer(),
                new Denormalizer\EventCategoryDenormalizer(),
                new Denormalizer\CandidateDenormalizer(),
                new Denormalizer\ProgramTopicDenormalizer(),
                new Denormalizer\ProjectDenormalizer(),
                new Denormalizer\IdeaCategoryDenormalizer(),
            ],
            [
                new JsonEncoder(),
            ]
        );
    }

    public function getProject(): Model\Project
    {
        $response = $this->request('GET', '/api/project');

        return $this->serializer->deserialize($response->getContent(), 'Project', 'json');
    }

    public function getSitemap(): array
    {
        return $this->request('GET', '/api/sitemap')->toArray();
    }

    public function getPostsCategories(): Model\Collection
    {
        $response = $this->request('GET', '/api/post_categories');
        if (200 !== $response->getStatusCode()) {
            return new Model\Collection([], new Model\Pagination(null, null, null, null));
        }

        return $this->serializer->deserialize($response->getContent(), 'hydra:Collection', 'json');
    }

    public function getPosts(int $page = 1, int $categoryFilter = null): Model\Collection
    {
        $url = '/api/posts?page='.$page;
        if ($categoryFilter) {
            $url .= '&categories='.$categoryFilter;
        }

        $response = $this->request('GET', $url);
        if (200 !== $response->getStatusCode()) {
            return new Model\Collection([], new Model\Pagination(null, null, null, null));
        }

        return $this->serializer->deserialize($response->getContent(), 'hydra:Collection', 'json');
    }

    public function getPost(int $postId, string $previewToken = null): ?Model\Post
    {
        $endpoint = '/api/posts/'.$postId;
        if ($previewToken) {
            $endpoint .= '?token='.$previewToken;
        }

        $response = $this->request('GET', $endpoint);
        if (200 !== $response->getStatusCode()) {
            return null;
        }

        return $this->serializer->deserialize($response->getContent(), 'Post', 'json');
    }

    public function getEventsCategories(): Model\Collection
    {
        $response = $this->request('GET', '/api/event_categories');
        if (200 !== $response->getStatusCode()) {
            return new Model\Collection([], new Model\Pagination(null, null, null, null));
        }

        return $this->serializer->deserialize($response->getContent(), 'hydra:Collection', 'json');
    }

    public function getExternalEvents(array $zipCodes = []): Model\Collection
    {
        $params = [];
        if ($zipCodes) {
            $params['zipCode'] = $zipCodes;
        }

        $response = $this->request('GET', '/api/external_events?'.http_build_query($params));
        if (200 !== $response->getStatusCode()) {
            return new Model\Collection([], new Model\Pagination(null, null, null, null));
        }

        return $this->serializer->deserialize($response->getContent(), 'hydra:Collection', 'json');
    }

    public function getEvents(array $options = []): Model\Collection
    {
        $params = [];

        if (isset($options['after'])) {
            $params['after'] = $options['after'];
        } elseif (isset($options['before'])) {
            $params['before'] = $options['before'];
        }

        if (isset($options['category'])) {
            $params['category'] = $options['category'];
        }

        $response = $this->request('GET', '/api/events?'.http_build_query($params));
        if (200 !== $response->getStatusCode()) {
            return new Model\Collection([], new Model\Pagination(null, null, null, null));
        }

        return $this->serializer->deserialize($response->getContent(), 'hydra:Collection', 'json');
    }

    public function getEvent(int $eventId, string $previewToken = null): ?Model\Event
    {
        $endpoint = '/api/events/'.$eventId;
        if ($previewToken) {
            $endpoint .= '?token='.$previewToken;
        }

        $response = $this->request('GET', $endpoint);
        if (200 !== $response->getStatusCode()) {
            return null;
        }

        return $this->serializer->deserialize($response->getContent(), 'Event', 'json');
    }

    public function getCandidates(): Model\Collection
    {
        $response = $this->request('GET', '/api/candidates');
        if (200 !== $response->getStatusCode()) {
            return new Model\Collection([], new Model\Pagination(null, null, null, null));
        }

        return $this->serializer->deserialize($response->getContent(), 'hydra:Collection', 'json');
    }

    public function getCandidate(int $candidateId, string $previewToken = null): ?Model\Candidate
    {
        $endpoint = '/api/candidates/'.$candidateId;
        if ($previewToken) {
            $endpoint .= '?token='.$previewToken;
        }

        $response = $this->request('GET', $endpoint);
        if (200 !== $response->getStatusCode()) {
            return null;
        }

        return $this->serializer->deserialize($response->getContent(), 'Candidate', 'json');
    }

    public function getProgramTopics(): Model\Collection
    {
        $response = $this->request('GET', '/api/program_topics');
        if (200 !== $response->getStatusCode()) {
            return new Model\Collection([], new Model\Pagination(null, null, null, null));
        }

        return $this->serializer->deserialize($response->getContent(), 'hydra:Collection', 'json');
    }

    public function getProgramTopic(int $themeId, string $previewToken = null): ?Model\ProgramTopic
    {
        $endpoint = '/api/program_topics/'.$themeId;
        if ($previewToken) {
            $endpoint .= '?token='.$previewToken;
        }

        $response = $this->request('GET', $endpoint);
        if (200 !== $response->getStatusCode()) {
            return null;
        }

        return $this->serializer->deserialize($response->getContent(), 'ProgramTopic', 'json');
    }

    public function createNewsletterSubscription(Model\NewsletterSubscription $subscription): bool
    {
        $response = $this->request('POST', '/api/newsletter_subscription_requests', 3, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            'json' => [
                'firstName' => $subscription->getFirstName(),
                'lastName' => $subscription->getLastName(),
                'email' => $subscription->getEmail(),
                'tags' => $subscription->getTags(),
                'botDetection' => $subscription->getBotDetection(),
            ],
        ]);

        return 202 === $response->getStatusCode();
    }

    public function getIdeasCategories(): Model\Collection
    {
        $response = $this->request('GET', '/api/idea_categories');
        if (200 !== $response->getStatusCode()) {
            return new Model\Collection([], new Model\Pagination(null, null, null, null));
        }

        return $this->serializer->deserialize($response->getContent(), 'hydra:Collection', 'json');
    }

    public function createIdea(Model\Idea $idea): bool
    {
        $response = $this->request('POST', '/api/idea_proposals', 3, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            'json' => [
                'firstName' => $idea->getFirstName(),
                'lastName' => $idea->getLastName(),
                'email' => $idea->getEmail(),
                'title' => $idea->getTitle(),
                'content' => $idea->getContent(),
                'contactBack' => $idea->getContactBack(),
                'subscribeNewsletter' => $idea->getSubscribeNewsletter(),
                'categories' => $idea->getCategories(),
                'botDetection' => $idea->getBotDetection(),
            ],
        ]);

        return 202 === $response->getStatusCode();
    }

    public function createDonation(Model\DonationRequest $request): bool
    {
        $response = $this->request('POST', '/api/donation_requests', 3, [
            'headers' => [
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ],
            'json' => [
                'amount' => $request->getAmount(),
                'civility' => $request->getCivility(),
                'firstName' => $request->getFirstName(),
                'lastName' => $request->getLastName(),
                'nationality' => $request->getNationality(),
                'phone' => $request->getPhone() ?: '',
                'email' => $request->getEmail() ?: '',
                'addressNumber' => $request->getAddressNumber() ?: '',
                'addressStreet' => $request->getAddressStreet(),
                'addressPostalCode' => $request->getAddressPostalCode(),
                'addressCity' => $request->getAddressCity(),
                'addressCountry' => $request->getAddressCountry(),
                'subscribeNewsletter' => $request->getSubscribeNewsletter(),
                'botDetection' => $request->getBotDetection(),
            ],
        ]);

        return 202 === $response->getStatusCode();
    }

    /**
     * Requests an HTTP resource.
     *
     * If a "503 Service Unavailable" status code is returned,
     * the request will be retried.
     */
    public function request(string $method, string $url, int $maxTrials = 3, array $options = []): ResponseInterface
    {
        $trials = 0;

        while (true) {
            ++$trials;
            $response = $this->httpClient->request($method, $url, $options);

            if (503 !== $response->getStatusCode()) {
                break;
            }

            if ($trials >= $maxTrials) {
                throw new MaintenanceException();
            }

            // 300ms
            usleep(300000);
        }

        return $response;
    }
}
