<?php

namespace CitePolitique\Sdk\Client;

/**
 * Entrypoint of the Cité Politique SDK client.
 */
interface ClientInterface
{
    public function getProject(): Model\Project;

    public function getSitemap(): array;

    /**
     * @return Model\Collection|Model\PostCategory[]
     */
    public function getPostsCategories(): Model\Collection;

    /**
     * @param int $categoryFilter
     *
     * @return Model\Collection|Model\Post[]
     */
    public function getPosts(int $page = 1, int $categoryFilter = null): Model\Collection;

    public function getPost(int $postId, string $previewToken = null): ?Model\Post;

    /**
     * @return Model\Collection|Model\EventCategory[]
     */
    public function getEventsCategories(): Model\Collection;

    /**
     * @return Model\Collection|Model\ExternalEvent[]
     */
    public function getExternalEvents(array $zipCodes = []): Model\Collection;

    /**
     * @return Model\Collection|Model\Event[]
     */
    public function getEvents(array $options = []): Model\Collection;

    public function getEvent(int $eventId, string $previewToken = null): ?Model\Event;

    /**
     * @return Model\Collection|Model\Candidate[]
     */
    public function getCandidates(): Model\Collection;

    public function getCandidate(int $candidateId, string $previewToken = null): ?Model\Candidate;

    /**
     * @return Model\Collection|Model\ProgramTopic[]
     */
    public function getProgramTopics(): Model\Collection;

    public function getProgramTopic(int $topicId, string $previewToken = null): ?Model\ProgramTopic;

    public function createNewsletterSubscription(Model\NewsletterSubscription $subscription): bool;

    /**
     * @return Model\Collection|Model\IdeaCategory[]
     */
    public function getIdeasCategories(): Model\Collection;

    public function createIdea(Model\Idea $idea): bool;

    public function createDonation(Model\DonationRequest $request): bool;
}
