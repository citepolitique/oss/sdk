<?php

namespace CitePolitique\Sdk\Client\Exception;

class MaintenanceException extends \RuntimeException
{
}
