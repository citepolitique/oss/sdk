<?php

namespace CitePolitique\Sdk\Client\Model;

class IdeaCategory
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var int
     */
    private $weight;

    /**
     * @var string|null
     */
    private $image;

    public function __construct(int $id, string $name, string $slug, int $weight, ?string $image)
    {
        $this->id = $id;
        $this->name = $name;
        $this->slug = $slug;
        $this->weight = $weight;
        $this->image = $image;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }
}
