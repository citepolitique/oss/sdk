<?php

namespace CitePolitique\Sdk\Client\Model;

class NewsletterSubscription
{
    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string[]
     */
    private $tags;

    /**
     * @var array
     */
    private $botDetection;

    public function __construct(string $firstName, string $lastName, string $email, array $botDetection = [], array $tags = [])
    {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->botDetection = $botDetection;
        $this->tags = $tags;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getBotDetection(): array
    {
        return $this->botDetection;
    }

    public function getTags(): array
    {
        return $this->tags;
    }
}
