<?php

namespace CitePolitique\Sdk\Client\Model;

class Project
{
    private $id;
    private $modules;
    private $settings;
    private $integrations;
    private $pages;
    private $menu;

    public function __construct(int $id, array $modules, array $settings, array $integrations, array $pages, array $menu)
    {
        $this->id = $id;
        $this->modules = $modules;
        $this->settings = $settings;
        $this->integrations = $integrations;
        $this->pages = $pages;
        $this->menu = $menu;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getModules(): array
    {
        return $this->modules;
    }

    public function getSettings(): array
    {
        return $this->settings;
    }

    public function getIntegrations(): array
    {
        return $this->integrations;
    }

    public function getPage(string $slug): ?Page
    {
        return $this->pages[$slug] ?? null;
    }

    /**
     * @return Page[]
     */
    public function getPages(): array
    {
        return $this->pages;
    }

    /**
     * @return MenuItem[]
     */
    public function getMenu(): array
    {
        return $this->menu;
    }
}
