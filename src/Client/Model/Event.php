<?php

namespace CitePolitique\Sdk\Client\Model;

class Event
{
    private $id;
    private $title;
    private $slug;
    private $description;
    private $image;
    private $date;
    private $url;
    private $buttonText;
    private $latitude;
    private $longitude;
    private $address;
    private $categories;
    private $updatedAt;
    private $publishedAt;

    public function __construct(
        int $id,
        string $title,
        string $slug,
        ?string $description,
        ?string $image,
        ?\DateTimeInterface $date,
        ?string $url,
        ?string $buttonText,
        ?string $latitude,
        ?string $longitude,
        ?string $address,
        array $categories,
        \DateTimeInterface $updatedAt,
        ?\DateTimeInterface $publishedAt
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->slug = $slug;
        $this->description = $description;
        $this->image = $image;
        $this->date = $date;
        $this->url = $url;
        $this->buttonText = $buttonText;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->address = $address;
        $this->categories = $categories;
        $this->updatedAt = $updatedAt;
        $this->publishedAt = $publishedAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function isFinished(): bool
    {
        return $this->date < new \DateTime();
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function getButtonText(): ?string
    {
        return $this->buttonText;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return EventCategory[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function isPublished(): bool
    {
        return $this->publishedAt && $this->publishedAt < new \DateTime();
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }
}
