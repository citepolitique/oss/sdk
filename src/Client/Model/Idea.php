<?php

namespace CitePolitique\Sdk\Client\Model;

class Idea
{
    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @var bool
     */
    private $contactBack;

    /**
     * @var bool
     */
    private $subscribeNewsletter;

    /**
     * @var array
     */
    private $categories;

    /**
     * @var array
     */
    private $botDetection;

    public function __construct(
        string $firstName,
        string $lastName,
        string $email,
        string $title,
        string $content,
        bool $contactBack,
        bool $subscribeNewsletter,
        array $categories = [],
        array $botDetection = []
    ) {
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->email = $email;
        $this->title = $title;
        $this->content = $content;
        $this->contactBack = $contactBack;
        $this->subscribeNewsletter = $subscribeNewsletter;
        $this->categories = $categories;
        $this->botDetection = $botDetection;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getContactBack(): bool
    {
        return $this->contactBack;
    }

    public function getSubscribeNewsletter(): bool
    {
        return $this->subscribeNewsletter;
    }

    /**
     * @return string[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    public function getBotDetection(): array
    {
        return $this->botDetection;
    }
}
