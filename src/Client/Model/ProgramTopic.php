<?php

namespace CitePolitique\Sdk\Client\Model;

class ProgramTopic
{
    private $id;
    private $title;
    private $slug;
    private $position;
    private $image;
    private $color;
    private $proposals;
    private $updatedAt;
    private $publishedAt;

    public function __construct(
        int $id,
        string $title,
        string $slug,
        ?array $proposals,
        ?int $position,
        ?string $image,
        ?string $color,
        \DateTimeInterface $updatedAt,
        ?\DateTimeInterface $publishedAt
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->slug = $slug;
        $this->position = $position;
        $this->image = $image;
        $this->color = $color;
        $this->proposals = $proposals;
        $this->updatedAt = $updatedAt;
        $this->publishedAt = $publishedAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getProposals(): ?array
    {
        return $this->proposals;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function isPublished(): bool
    {
        return $this->publishedAt && $this->publishedAt < new \DateTime();
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }
}
