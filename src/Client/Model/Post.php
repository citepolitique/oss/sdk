<?php

namespace CitePolitique\Sdk\Client\Model;

class Post
{
    private $id;
    private $title;
    private $slug;
    private $description;
    private $metaTitle;
    private $metaDescription;
    private $content;
    private $image;
    private $video;
    private $categories;
    private $updatedAt;
    private $publishedAt;

    public function __construct(
        int $id,
        string $title,
        string $slug,
        ?string $description,
        ?string $metaTitle,
        ?string $metaDescription,
        ?array $content,
        ?string $image,
        ?string $video,
        array $categories,
        \DateTimeInterface $updatedAt,
        ?\DateTimeInterface $publishedAt
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->slug = $slug;
        $this->description = $description;
        $this->metaTitle = $metaTitle;
        $this->metaDescription = $metaDescription;
        $this->content = $content;
        $this->image = $image;
        $this->video = $video;
        $this->categories = $categories;
        $this->updatedAt = $updatedAt;
        $this->publishedAt = $publishedAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getDescription(): ?string
    {
        return $this->metaDescription ?: $this->description ?: null;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function getContent(): ?array
    {
        return $this->content;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getVideo(): ?string
    {
        return $this->video;
    }

    /**
     * @return PostCategory[]
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function isPublished(): bool
    {
        return $this->publishedAt && $this->publishedAt < new \DateTime();
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }
}
