<?php

namespace CitePolitique\Sdk\Client\Model;

class Candidate
{
    private $id;
    private $fullName;
    private $slug;
    private $content;
    private $description;
    private $position;
    private $image;
    private $facebook;
    private $twitter;
    private $email;
    private $updatedAt;
    private $publishedAt;

    public function __construct(
        int $id,
        string $fullName,
        string $slug,
        ?array $content,
        ?string $description,
        ?int $position,
        ?string $image,
        ?string $facebook,
        ?string $twitter,
        ?string $email,
        \DateTimeInterface $updatedAt,
        ?\DateTimeInterface $publishedAt
    ) {
        $this->id = $id;
        $this->fullName = $fullName;
        $this->slug = $slug;
        $this->content = $content;
        $this->description = $description;
        $this->position = $position;
        $this->image = $image;
        $this->facebook = $facebook;
        $this->twitter = $twitter;
        $this->email = $email;
        $this->updatedAt = $updatedAt;
        $this->publishedAt = $publishedAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFullName(): string
    {
        return $this->fullName;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getContent(): ?array
    {
        return $this->content;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function getFacebook(): ?string
    {
        return $this->facebook;
    }

    public function getTwitter(): ?string
    {
        return $this->twitter;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function isPublished(): bool
    {
        return $this->publishedAt && $this->publishedAt < new \DateTime();
    }

    public function getPublishedAt(): ?\DateTimeInterface
    {
        return $this->publishedAt;
    }
}
