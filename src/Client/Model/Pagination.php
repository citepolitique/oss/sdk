<?php

namespace CitePolitique\Sdk\Client\Model;

class Pagination
{
    private $totalItems;
    private $first;
    private $last;
    private $previous;
    private $next;

    public function __construct(
        ?int $totalItems,
        ?string $first = null,
        ?string $last = null,
        ?string $previous = null,
        ?string $next = null
    ) {
        $this->totalItems = $totalItems;
        $this->first = $first;
        $this->last = $last;
        $this->previous = $previous;
        $this->next = $next;
    }

    public function getTotalItems(): ?int
    {
        return $this->totalItems;
    }

    public function getFirst(): ?string
    {
        return $this->first;
    }

    public function getLast(): ?string
    {
        return $this->last;
    }

    public function getPrevious(): ?string
    {
        return $this->previous;
    }

    public function getNext(): ?string
    {
        return $this->next;
    }
}
