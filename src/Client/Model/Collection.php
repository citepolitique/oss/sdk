<?php

namespace CitePolitique\Sdk\Client\Model;

class Collection implements \IteratorAggregate
{
    private $items;
    private $pagination;

    public function __construct(array $items, Pagination $pagination)
    {
        $this->items = $items;
        $this->pagination = $pagination;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->items);
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function getPagination(): Pagination
    {
        return $this->pagination;
    }
}
