<?php

namespace CitePolitique\Sdk\Client\Model;

class Page
{
    private $id;
    private $title;
    private $slug;
    private $metaTitle;
    private $metaDescription;
    private $deletable;
    private $content;
    private $updatedAt;

    public function __construct(
        int $id,
        string $title,
        string $slug,
        ?string $metaTitle,
        ?string $metaDescription,
        bool $deletable,
        array $content,
        \DateTimeInterface $updatedAt
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->slug = $slug;
        $this->metaTitle = $metaTitle;
        $this->metaDescription = $metaDescription;
        $this->deletable = $deletable;
        $this->content = $content;
        $this->updatedAt = $updatedAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function isDeletable(): bool
    {
        return $this->deletable;
    }

    public function getContent(): ?array
    {
        return $this->content;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }
}
