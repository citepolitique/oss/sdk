<?php

namespace CitePolitique\Sdk\Client\Model;

class ExternalEvent
{
    private $id;
    private $source;
    private $sourceId;
    private $title;
    private $slug;
    private $date;
    private $url;
    private $latitude;
    private $longitude;
    private $address;
    private $zipCode;
    private $city;
    private $country;
    private $updatedAt;

    public function __construct(
        int $id,
        string $source,
        string $sourceId,
        string $title,
        string $slug,
        ?\DateTime $date,
        ?string $url,
        ?string $latitude,
        ?string $longitude,
        ?string $address,
        ?string $zipCode,
        ?string $city,
        ?string $country,
        \DateTime $updatedAt
    ) {
        $this->id = $id;
        $this->source = $source;
        $this->sourceId = $sourceId;
        $this->title = $title;
        $this->slug = $slug;
        $this->date = $date;
        $this->url = $url;
        $this->latitude = $latitude;
        $this->longitude = $longitude;
        $this->address = $address;
        $this->zipCode = $zipCode;
        $this->city = $city;
        $this->country = $country;
        $this->updatedAt = $updatedAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSource(): string
    {
        return $this->source;
    }

    public function getSourceId(): string
    {
        return $this->sourceId;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function getLatitude(): ?string
    {
        return $this->latitude;
    }

    public function getLongitude(): ?string
    {
        return $this->longitude;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }
}
