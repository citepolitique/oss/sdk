<?php

namespace CitePolitique\Sdk\Client\Model;

class DonationRequest
{
    /**
     * @var int
     */
    private $amount;

    /**
     * @var string
     */
    private $civility;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $nationality;

    /**
     * @var string|null
     */
    private $addressNumber;

    /**
     * @var string
     */
    private $addressStreet;

    /**
     * @var string
     */
    private $addressPostalCode;

    /**
     * @var string
     */
    private $addressCity;

    /**
     * @var string
     */
    private $addressCountry;

    /**
     * @var string|null
     */
    private $phone;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var bool
     */
    private $subscribeNewsletter = false;

    /**
     * @var array
     */
    private $botDetection;

    public function __construct(
        int $amount,
        string $civility,
        string $firstName,
        string $lastName,
        string $nationality,
        ?string $addressNumber,
        string $addressStreet,
        string $addressPostalCode,
        string $addressCity,
        string $addressCountry,
        ?string $phone,
        ?string $email,
        bool $subscribeNewsletter,
        array $botDetection
    ) {
        $this->amount = $amount;
        $this->civility = $civility;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->nationality = $nationality;
        $this->addressNumber = $addressNumber;
        $this->addressStreet = $addressStreet;
        $this->addressPostalCode = $addressPostalCode;
        $this->addressCity = $addressCity;
        $this->addressCountry = $addressCountry;
        $this->phone = $phone;
        $this->email = $email;
        $this->subscribeNewsletter = $subscribeNewsletter;
        $this->botDetection = $botDetection;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getCivility(): string
    {
        return $this->civility;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function getNationality(): string
    {
        return $this->nationality;
    }

    public function getAddressNumber(): ?string
    {
        return $this->addressNumber;
    }

    public function getAddressStreet(): string
    {
        return $this->addressStreet;
    }

    public function getAddressPostalCode(): string
    {
        return $this->addressPostalCode;
    }

    public function getAddressCity(): string
    {
        return $this->addressCity;
    }

    public function getAddressCountry(): string
    {
        return $this->addressCountry;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getSubscribeNewsletter(): bool
    {
        return $this->subscribeNewsletter;
    }

    public function getBotDetection(): array
    {
        return $this->botDetection;
    }
}
