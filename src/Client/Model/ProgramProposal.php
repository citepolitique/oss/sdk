<?php

namespace CitePolitique\Sdk\Client\Model;

class ProgramProposal
{
    private $id;
    private $title;
    private $content;
    private $position;
    private $updatedAt;

    public function __construct(
        int $id,
        string $title,
        ?array $content,
        ?int $position,
        \DateTimeInterface $updatedAt
    ) {
        $this->id = $id;
        $this->title = $title;
        $this->content = $content;
        $this->position = $position;
        $this->updatedAt = $updatedAt;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): ?array
    {
        return $this->content;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }
}
