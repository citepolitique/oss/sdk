<?php

namespace CitePolitique\Sdk\Client\Model;

class EventCategory
{
    private $id;
    private $name;
    private $slug;
    private $weight;

    public function __construct(int $id, string $name, string $slug, int $weight)
    {
        $this->id = $id;
        $this->name = $name;
        $this->slug = $slug;
        $this->weight = $weight;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getWeight(): int
    {
        return $this->weight;
    }
}
