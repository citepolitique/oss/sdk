<?php

namespace CitePolitique\Sdk\Client\Model;

class MenuItem
{
    private $id;
    private $label;
    private $type;
    private $openNewTab;
    private $extra;

    public function __construct(int $id, string $label, string $type, bool $openNewTab, array $extra = [])
    {
        $this->id = $id;
        $this->label = $label;
        $this->type = $type;
        $this->openNewTab = $openNewTab;
        $this->extra = $extra;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getOpenNewTab(): bool
    {
        return $this->openNewTab;
    }

    public function getExtra(): array
    {
        return $this->extra;
    }
}
