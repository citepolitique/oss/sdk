<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

class FacebookBlockHandler implements BlockHandlerInterface
{
    use BlockHandlerTrait;

    public function getSupportedBlockTypes(): array
    {
        return ['facebook'];
    }

    public function handle(array $block): string
    {
        if (!isset($block['data']['url'])) {
            return '';
        }

        return
            '<div class="embed-block embed-facebook">'.
                '<div data-href="'.$block['data']['url'].'" data-width="500" data-show-text="true" class="fb-post"></div>'.
            '</div>'
        ;
    }

    public function getPostBuildHtml(): string
    {
        return
            '<div id="fb-root"></div>'.
            '<script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.3"></script>'
        ;
    }
}
