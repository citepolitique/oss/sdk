<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

class ImageBlockHandler implements BlockHandlerInterface
{
    use BlockHandlerTrait;

    public function getSupportedBlockTypes(): array
    {
        return ['image'];
    }

    public function handle(array $block): string
    {
        if (!isset($block['data']['file']['url'])) {
            return '';
        }

        $size = '';

        if ($width = $block['data']['file']['width'] ?? null) {
            $size .= 'width="'.$width.'" ';
        }

        if ($height = $block['data']['file']['height'] ?? null) {
            $size .= 'height="'.$height.'" ';
        }

        $caption = $block['data']['caption'] ?? '';

        return
            '<figure class="figure">'.
                '<img src="'.$block['data']['file']['url'].'" '.$size.'/>'.
                ($caption ? '<figcaption>'.$caption.'</figcaption>' : '').
            '</figure>'
        ;
    }
}
