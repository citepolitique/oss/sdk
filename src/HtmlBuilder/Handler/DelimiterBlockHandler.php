<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

class DelimiterBlockHandler implements BlockHandlerInterface
{
    use BlockHandlerTrait;

    public function getSupportedBlockTypes(): array
    {
        return ['delimiter'];
    }

    public function handle(array $block): string
    {
        return '<hr />';
    }
}
