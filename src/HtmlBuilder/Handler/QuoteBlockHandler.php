<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

class QuoteBlockHandler implements BlockHandlerInterface
{
    use BlockHandlerTrait;

    public function getSupportedBlockTypes(): array
    {
        return ['quote'];
    }

    public function handle(array $block): string
    {
        if (!isset($block['data']['text'])) {
            return '';
        }

        $caption = $block['data']['caption'] ?? '';

        return
            '<blockquote class="blockquote blockquote-'.($block['data']['alignment'] ?? 'left').'">'.
                $block['data']['text'].
                ($caption ? '<footer>'.$caption.'</footer>' : '').
            '</blockquote>'
        ;
    }
}
