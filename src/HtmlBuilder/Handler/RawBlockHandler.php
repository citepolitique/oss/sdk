<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

class RawBlockHandler implements BlockHandlerInterface
{
    use BlockHandlerTrait;

    public function getSupportedBlockTypes(): array
    {
        return ['raw'];
    }

    public function handle(array $block): string
    {
        return $block['data']['html'] ?? '';
    }
}
