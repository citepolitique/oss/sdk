<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

class InstagramBlockHandler implements BlockHandlerInterface
{
    use BlockHandlerTrait;

    public function getSupportedBlockTypes(): array
    {
        return ['instagram'];
    }

    public function handle(array $block): string
    {
        if (!isset($block['data']['url'])) {
            return '';
        }

        return
            '<div class="embed-block embed-instagram">'.
                '<blockquote class="instagram-media" data-instgrm-captioned data-instgrm-version="12" '.
                            'data-instgrm-permalink="'.$block['data']['url'].'" '.
                            'style="background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:540px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">'.
                '</blockquote>'.
            '</div>'
        ;
    }

    public function getPostBuildHtml(): string
    {
        return '<script async src="//www.instagram.com/embed.js"></script>';
    }
}
