<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

class TwitterBlockHandler implements BlockHandlerInterface
{
    use BlockHandlerTrait;

    public function getSupportedBlockTypes(): array
    {
        return ['twitter'];
    }

    public function handle(array $block): string
    {
        if (!isset($block['data']['url'])) {
            return '';
        }

        return
            '<div class="embed-block embed-twitter">'.
                '<blockquote class="twitter-tweet" data-lang="fr">'.
                    '<a href="'.$block['data']['url'].'"></a>'.
                '</blockquote>'.
            '</div>'
        ;
    }

    public function getPostBuildHtml(): string
    {
        return '<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>';
    }
}
