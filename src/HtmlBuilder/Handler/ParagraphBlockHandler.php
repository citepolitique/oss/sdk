<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

class ParagraphBlockHandler implements BlockHandlerInterface
{
    use BlockHandlerTrait;

    public function getSupportedBlockTypes(): array
    {
        return ['paragraph'];
    }

    public function handle(array $block): string
    {
        if (!isset($block['data']['text'])) {
            return '';
        }

        return '<p>'.$block['data']['text'].'</p>';
    }
}
