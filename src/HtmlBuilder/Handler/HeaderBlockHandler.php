<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

class HeaderBlockHandler implements BlockHandlerInterface
{
    use BlockHandlerTrait;

    private $counter = 0;

    public function getSupportedBlockTypes(): array
    {
        return ['header'];
    }

    public function handle(array $block): string
    {
        if (!isset($block['data']['text'])) {
            return '';
        }

        $level = 2;
        if (isset($block['data']['level']) && $block['data']['level'] > 0 && $block['data']['level'] <= 6) {
            $level = $block['data']['level'];
        }

        ++$this->counter;

        return '<h'.$level.' id="title-'.$this->counter.'">'.$block['data']['text'].'</h'.$level.'>';
    }
}
