<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

class ListBlockHandler implements BlockHandlerInterface
{
    use BlockHandlerTrait;

    public function getSupportedBlockTypes(): array
    {
        return ['list'];
    }

    public function handle(array $block): string
    {
        if (!isset($block['data']['items'])) {
            return '';
        }

        $tag = (isset($block['data']['style']) && 'ordered' === $block['data']['style']) ? 'ol' : 'ul';

        $html = '<'.$tag.'>'."\n";
        foreach ($block['data']['items'] as $item) {
            $html .= '<li>'.$item.'</li>'."\n";
        }

        $html .= '</'.$tag.'>';

        return $html;
    }
}
