<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

interface BlockHandlerInterface
{
    /**
     * Return the list of block types supported by this handler.
     *
     * @return string[]
     */
    public function getSupportedBlockTypes(): array;

    /**
     * Allows handlers to add specific HTML at the end of the result.
     * Called once, after the build of all the blocks, only on handlers that were used for the build.
     */
    public function getPostBuildHtml(): string;

    /**
     * Handle a block to transform it into HTML.
     */
    public function handle(array $block): string;
}
