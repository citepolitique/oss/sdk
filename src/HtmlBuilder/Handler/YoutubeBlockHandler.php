<?php

namespace CitePolitique\Sdk\HtmlBuilder\Handler;

class YoutubeBlockHandler implements BlockHandlerInterface
{
    private const ID_REGEX = '/^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*/';

    use BlockHandlerTrait;

    public function getSupportedBlockTypes(): array
    {
        return ['youtube'];
    }

    public function handle(array $block): string
    {
        if (!isset($block['data']['url'])) {
            return '';
        }

        preg_match(self::ID_REGEX, $block['data']['url'], $matches);

        $id = $matches[1] ?? null;
        if (!$id) {
            return '';
        }

        return
            '<div class="embed-block embed-youtube">'.
                '<iframe width="560" height="315" frameborder="0" '.
                        'src="https://www.youtube-nocookie.com/embed/'.$id.'" '.
                        'allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" '.
                        'allowfullscreen></iframe>'.
            '</div>'
        ;
    }
}
