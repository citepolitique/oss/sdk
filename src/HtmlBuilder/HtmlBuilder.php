<?php

namespace CitePolitique\Sdk\HtmlBuilder;

use CitePolitique\Sdk\HtmlBuilder\Handler\BlockHandlerInterface;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class HtmlBuilder implements HtmlBuilderInterface
{
    /**
     * @var BlockHandlerInterface[]
     */
    private $blockHandlers;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param BlockHandlerInterface[] $blockHandlers
     */
    public function __construct(iterable $blockHandlers, LoggerInterface $logger = null)
    {
        $this->blockHandlers = [];
        foreach ($blockHandlers as $handler) {
            foreach ($handler->getSupportedBlockTypes() as $supportedType) {
                $this->blockHandlers[$supportedType] = $handler;
            }
        }

        $this->logger = $logger ?: new NullLogger();
    }

    public function buildHtml(array $content): string
    {
        $calledHandlers = [];

        $html = '';
        foreach ($content['blocks'] ?? [] as $block) {
            if (!($handler = $this->blockHandlers[$block['type']] ?? null)) {
                $this->logger->error('The content block of type "'.$block['type'].'" is not supported.');

                continue;
            }

            $calledHandlers[\get_class($handler)] = $handler;
            $html .= $handler->handle($block)."\n";
        }

        foreach ($calledHandlers as $handler) {
            $html .= $handler->getPostBuildHtml($content, $html)."\n";
        }

        return $html;
    }
}
