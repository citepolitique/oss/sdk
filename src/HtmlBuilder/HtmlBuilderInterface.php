<?php

namespace CitePolitique\Sdk\HtmlBuilder;

/**
 * Entrypoint of the Cité Politique HTML builder.
 */
interface HtmlBuilderInterface
{
    public function buildHtml(array $content): string;
}
