# Cité Politique integration for Symfony

Install this package, enable the bundle if Symfony Flex didn't do it for you
and create a `config/packages/citepolitique.yaml` file:

```yaml
# config/packages/citepolitique.yaml
cite_politique:
    clients:
        demoClient:
            endpoint: 'https://console-demo.citepolitique.fr'
            api_token: '1c8b9da589a23ff9536ac7cd8603e4f740c5da25c4e588394d728188c426f375'
```
